package com.example.authentication

import android.content.pm.SigningInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.util.Log.w
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_sign_in.*
import kotlinx.android.synthetic.main.activity_sign_up_activty.*

class SignInActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        init()
    }

    private fun init() {
        auth = Firebase.auth

        Sign_In_Button.setOnClickListener {
            signin()
        }
    }

    private fun signin() {
        val SignInEmail = eMailEditText.text.toString()
        val SignInPassword = PasswordEditText.text.toString()
        if (SignInEmail.isNotEmpty() && SignInPassword.isNotEmpty()) {
            signinprogressbar.visibility = View.VISIBLE
            auth.signInWithEmailAndPassword(SignInEmail, SignInPassword)
                .addOnCompleteListener(this) { task ->
                    signinprogressbar.visibility = View.GONE

                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        d("signin", "signInWithEmail:success")
                        Toast.makeText(this, "Authentication is Success!", Toast.LENGTH_SHORT)
                            .show()
                        val user = auth.currentUser
                    } else {
                        // If sign in fails, display a message to the user.
                        d("signin", "signInWithEmail:failure", task.exception)
                        Toast.makeText(
                            baseContext, "Authentication failed.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
        }else{
            Toast.makeText(this, "Please fill both fields", Toast.LENGTH_SHORT).show()
        }
    }
}