package com.example.authentication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_sign_up_activty.*

class SignUpActivty : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up_activty)
        init()
    }

    fun isEmailValid(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }


    private fun init() {
        auth = Firebase.auth

        signUpButton.setOnClickListener {
            signup()
        }
    }

    private fun signup() {
        val email = emailEditText.text.toString()
        val password = passwordEditText.text.toString()
        val repeatPassword = repeatPasswordEditText.text.toString()



        if (email.isNotEmpty() && password.isNotEmpty() && repeatPassword.isNotEmpty()) {
            if (password == repeatPassword) {
                progressBar.visibility = View.VISIBLE
                signUpButton.isClickable = false
                auth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this) { task ->
                        progressBar.visibility = View.GONE
                        signUpButton.isClickable = true


                        if (task.isSuccessful) {
                            d("signup", "SignUp is Success!")
                            Toast.makeText(this, "SignUp is Success", Toast.LENGTH_SHORT)
                                .show()
                            val user = auth.currentUser
                        }else if(isEmailValid(email)!=true){

                            Toast.makeText(this, "“Email format is not Correct", Toast.LENGTH_SHORT).show()
                        }else {
                            d("signup", "createUserWithEmail:failure", task.exception)
                            Toast.makeText(
                                baseContext, "Authentication failed.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }

            }else {
                Toast.makeText(this, "Passwords do not match.Try again", Toast.LENGTH_SHORT).show()
            }

        } else { Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show() }
    }
}
